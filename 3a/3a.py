import matplotlib.pyplot as plt

a = []
n = int(input())
while (n != 1):
    if (n % 2 == 0):
        n/=2
        a.append(n)
    else:
        n = n * 3 + 1
        a.append(n)
fig, ax = plt.subplots()
ax.plot(a) 